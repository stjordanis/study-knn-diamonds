"""
This code makes use of the Cottonwood machine learning framework.
Get it at https://e2eml.school/cottonwood
"""
import time
import numpy as np
import matplotlib.pyplot as plt
from cottonwood.structure import Structure
from cottonwood.knn import KNN
from cottonwood_data_diamonds.diamonds_block import Data

k = 7
max_points = 10000
n_bin = int(1e4)

# Create the k-NN model in Cottonwood.
model = Structure()

# Initialize the blocks.
model.add(Data(), "data")
model.add(KNN(
    k=k,
    is_classifier=False,
    max_points=max_points,
    use_pretrained_weights=True,
), "knn")

# Hook them up together.
model.connect("data", "knn")
model.connect("data", "knn", i_port_tail=1, i_port_head=1)

start_time = time.time()
differences = []
for i_iter in range(int(model.blocks["data"].n_examples)):
    model.forward_pass()
    try:
        differences.append(np.abs(
            model.blocks["knn"].target_label -
            model.blocks["knn"].forward_out))
    except TypeError:
        # Initially some of the estimates are None. Ignore these
        pass
duration = time.time() - start_time
print(f"Run completed in {duration} seconds.")

differences = np.array(differences[k + 1:])
differences = differences[:(differences.size // n_bin) * n_bin]
differences = np.reshape(differences, (n_bin, -1), order="F")
mean_differences = np.mean(differences, axis=0)
pct = (10 ** mean_differences - 1) * 100

iters = np.arange(mean_differences.size) * n_bin
fig = plt.figure()
ax = fig.gca()
ax.plot(iters, pct, color="#04253a")
ax.set_title(f"k-NN with Diamond Prices, k = {model.blocks['knn'].k}")
ax.set_xlabel("Iteration")
ax.set_ylabel("Mean percentage price error")
ax.grid()
plt.savefig("learning_curve.png", dpi=300)
plt.show()
